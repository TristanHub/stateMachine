package com.example.demo.config;



import java.util.Arrays;
import java.util.HashSet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.guard.Guard;


@Configuration
@EnableStateMachine
public class SimpleStateMachineConfiguration extends StateMachineConfigurerAdapter<String, String> {
    
    @Override
    public void configure(StateMachineStateConfigurer<String, String> states) throws Exception {
        states.withStates()
            .initial("SI", emailAction())
            .choice("CHOICE")
            .state("OK", validAction())
            .state("NOK", refuseAction())
            .end("end");
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<String, String> transitions) throws Exception {
        transitions.withExternal()
                        .source("SI")
                        .target("CHOICE")
                        .event("answer")
                        .and()
                    .withChoice()
                        .source("CHOICE")
                        .first("OK", validationGuard())
                        .last("NOK")
                        .and()
                    .withExternal()
                        .source("OK")
                        .target("end")
                        .and()
                    .withExternal()
                        .source("NOK")
                        .target("end");
    }

    @Bean
    public Action<String, String> initAction() {
        return ctx -> System.out.println("change " + ctx.getTarget().getId());
    }

    @Bean
    public Action<String, String> errorAction() {
        return ctx -> System.out.println("Error " + ctx.getSource().getId() + ctx.getException());
    }

    @Bean
    public Action<String, String> entryAction() {
        return ctx -> System.out.println("Entry " + ctx.getTarget().getId());
    }

    @Bean
    public Action<String, String> executeAction() {
        return ctx -> System.out.println("Do " + ctx.getTarget().getId());
    }

    @Bean
    public Action<String, String> exitAction() {
        return ctx -> System.out.println("Exit " + ctx.getSource().getId() + " -> " + ctx.getTarget().getId());
    }

    @Bean
    public Action<String, String> approvalAction() {
        return ctx -> {
            int approvals = (int) ctx.getExtendedState().getVariables()
                                .getOrDefault("approvalCount", 0);
            approvals++;
            ctx.getExtendedState().getVariables()
                .put("approvalCount", approvals);
        };
    }

    @Bean
    public Action<String, String> emailAction() {
        return ctx -> System.out.println("send Email with token validation");
    }

    @Bean
    public Action<String, String> validAction() {
        return ctx -> System.out.println("upload validated");
    }

    @Bean
    public Action<String, String> refuseAction() {
        return ctx -> System.out.println("upload refused");
    }

    @Bean
    public Action<String, String> getApproval() {
        return ctx -> System.out.println("approval = " + 
            (int) ctx.getExtendedState().getVariables().get("approvalCount"));
    }

    @Bean
    public Guard<String, String> simpleGuard() {
        return ctx -> {
            int approvalCount = (int) ctx
                .getExtendedState()
                .getVariables()
                .getOrDefault("approvalCount", 0);
                return approvalCount > 2;
        };
    }

    @Bean
    public Guard<String, String> validationGuard() {
        return ctx -> {
            return (boolean) ctx.getExtendedState()
                                .getVariables()
                                .getOrDefault("validation", false);
        };
    }

    @Bean
    public Guard<String, String> highGuard() {
        return ctx -> false;
    }

    @Bean
    public Guard<String, String> lowGuard() {
        return ctx -> true;
    }
}