package com.example.demo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.*;

@RestController
@RequestMapping(path="/")
public class PingController {

    @Autowired
    private StateMachine<String, String> stateMachine;

    @Autowired
    private StateContext context;

    @GetMapping(path="/ping")
    public String ping() {
        return "ping";
    }

    @GetMapping(path="/add")
    public String add() {
        stateMachine.sendEvent("end");
        return "added";
    }

    @PostMapping(path="/valid")
    public String valid() {
        context.getExtendedState().getVariables().put("validation", true);
        stateMachine.sendEvent("answer");
        return "valid";
    }

    @PostMapping(path="/refuse")
    public String refuse() {
        context.getExtendedState().getVariables().put("validation", false);
        stateMachine.sendEvent("answer");
        return "refuse";
    }
}