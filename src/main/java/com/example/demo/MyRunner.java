package com.example.demo;
//import com.example.demo.config.StateMachineConfig;
import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.statemachine.*;

@Component
public class MyRunner implements CommandLineRunner {
    
    @Autowired
    private StateMachine<String, String> stateMachine;

    @Override
    public void run(String... args) throws Exception {
        //stateMachine.sendEvent(Events.E2);
        //stateMachine.sendEvent(Events.E1);
        //stateMachine.sendEvent(Events.E2);
        stateMachine.start();
    }
}